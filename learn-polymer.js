import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `learn-polymer`
 * 
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class LearnPolymer extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h2>Hello [[prop1]]!</h2>
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'learn-polymer',
      },
    };
  }
}

window.customElements.define('learn-polymer', LearnPolymer);
